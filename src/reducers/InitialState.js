export class InitialState {  
  
  mode_type:'';
  user_name:'';
  response_message:'';
  chosen_drinks:[];
  chosen_flavours:[];
  new_drink:'';
  nectar_points:0;
  pour_drink:false;
  pouring:false;
  end_screen:false;
 
}

export default InitialState;