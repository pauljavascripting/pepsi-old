import React, { Component } from 'react';
import * as faceapi from 'face-api.js';
import { store } from '../index.js';
import faceFrame from '../images/face_frame.png'
import pepsiImage from '../images/face_frame_pepsi.png'
import ringsTextImage from '../images/rings_text.png'

import emir from '../images/emir.jpg'

let video
let interval
let interval2
let self
const faceDetectionScore = 0.35
const faceMatchScore = 0.4
const useTinyModel = true
const videoHeight = 1024;
const videoWidth = 600;
const MODEL_URL = process.env.PUBLIC_URL+'/models'
const JSON_URL = process.env.PUBLIC_URL+'/json/faces.json'
let faceDistance = 0
let faceName = ''
let boxX = 0
let boxY = 0
const inputSize = 128 // defines quality of detection
let width // used to define box width
let height // used to define box height

let loadedFlag = false

export class LoadVideoDetection extends Component {

	constructor(){

		super()

		this.state = {faceDetected:false, introImage:true, frameType:1, videoLoaded:false, intervalLoaded:false}

		interval = null
		
		self = this
	}


	getName(){

		return faceName;
	}

	setName(nm){

		faceName = nm
	}

	getDistance(){

		return faceDistance

	}

	setDistance(dist){

		faceDistance = dist
	}


	getBoxX(){

		return boxX
	}

	setBoxX(x){

		boxX = x
	}

	getBoxY(){

		return boxY
	}

	setBoxY(y){

		boxY = y
	}

	getBoxHeight(){

		return height
	}

	setBoxHeight(ht){

		height = ht
	}

	getBoxWidth(){

		return width
	}

	setBoxWidth(wd){

		width = wd
	}

	componentDidUnMount(){

		clearInterval(interval)
		interval = null
	}

	async componentDidMount(){

		// load facial models ---------------------------------------
		await faceapi.loadTinyFaceDetectorModel(MODEL_URL)
	    await faceapi.loadFaceLandmarkTinyModel(MODEL_URL)
    	await faceapi.loadFaceRecognitionModel(MODEL_URL)

    	// add video ---------------------------------------
		var constraints = { audio: false, video: { width: videoWidth, height: videoHeight } }; 
	    navigator.mediaDevices.getUserMedia(constraints)
	    .then(function(mediaStream) {

	      //video = document.querySelector('video');
	      video = document.getElementById('video')
	      video.srcObject = mediaStream;
	        
	        // video on loaded ---------------------------------------
	        video.onloadedmetadata = function(e) {

	          // play video
	          video.play();

	          self.setState({ videoLoaded:true })

	        }
	    })

	}

	static getDerivedStateFromProps(nextProps){
		
		if(nextProps.modeType==='passive' && loadedFlag===false ){

			//interval = setInterval(self.drawBox, 200) // start calling drawBox() once passive mode has been removed

			//self.getDataFromFace()

			loadedFlag = true
				
			interval = setInterval(self.drawBox, 1000)
			

			//interval = setInterval(self.startDrawBox, 150)
		}

		return false

	}

	startTimer(){

		interval = setInterval(self.startDrawBox, 150)
	}


	async getDataFromFace(){

		let input = self.refs.faceImg // embed image in render()

		const result = await faceapi.detectSingleFace(input,new faceapi.TinyFaceDetectorOptions({inputSize: inputSize })).withFaceLandmarks(useTinyModel).withFaceDescriptor()

        console.log(result.descriptor.join("\",\""))

	}

	startDrawBox(){

		clearInterval(interval)
		interval = null

		interval = setInterval(self.drawBox, 1000)

	}

	async drawBox(){

		if(self.state.videoLoaded){

			 

			let dataLoaded=0
			// let input = document.getElementById('video')
			let input = self.refs.person
			const result = await faceapi.detectSingleFace(input,new faceapi.TinyFaceDetectorOptions({inputSize: inputSize })).withFaceLandmarks(useTinyModel).withFaceDescriptor()
			
			if(result && dataLoaded===0){

				 // hide intro message -----------------------------------
			    if(self.state.introImage){
			    	let img = self.refs.imageRingsText
					img.style.visibility = 'hidden'
					self.setState({introImage:false})

			    }
				
				dataLoaded = 1

				console.log(result.descriptor.join("\",\"").replace(/['"]+/g, ''))

				console.log('-----------------')

				clearInterval(interval)
				interval = null

			}
			

		}
		else{

			console.log('waiting for data.....')
		}
	    
	}

	drawImageToCanvas(num, x, y, width, height){

		let img
		let incrementX = 50
		let incrementY = 150

		// draw image frame onto canvas
	    const canvas = self.refs.canvas
	    const ctx = canvas.getContext("2d");
	    canvas.width = videoWidth
	    canvas.height = videoHeight
	    
	    switch(self.state.frameType){

	    	case 1:

	    		 img = self.refs.imageFrame

	    	break;

	    	case 2:

	    		 img = self.refs.imagePepsi
	    		 incrementX = 50
	    		 incrementY = 200

	    	break;
	    	default:

	    		 img = self.refs.imageFrame

	    	break

	    }

	    // draw image
	    // ctx.drawImage(img, self.getBoxX()+incrementX, self.getBoxY()-incrementY, img.width*ratio, img.height*ratio);
	    ctx.drawImage(img, self.getBoxX()+incrementX, self.getBoxY()-incrementY, 500, 700);

	}


	async checkMatch(img1){

		if(img1[0]){

			 //add first image to facematch
			 const faceMatcher = new faceapi.FaceMatcher(img1)

			 // load json
			 const json = await faceapi.fetchJson(JSON_URL)

			 // pick specific face from json
			  //var data = new Float32Array(json['Paul'].descriptors);
			  //const faceMatcher = new faceapi.FaceMatcher(data)

			 // loop json
			 let array = Object.values(json);
       		 let bestMatch

			for (let i = 0; i < array.length; i++) {
				//console.log(i)
				bestMatch = faceMatcher.findBestMatch(array[i].descriptors)
				//console.log(bestMatch._distance)
				if(bestMatch._distance<faceMatchScore){

					//console.log(array[i].name)
					self.completeMatch(array[i].name, bestMatch._distance)
					break

				}
				else{

					if(i===array.length-1){

						//console.log('no match end of loop!!!')

		    	 		self.setState({ faceDetected:false })

					}
				}

				
			}
			  
			
		}
		else{

			//console.log('restart--------')

	         // disable
	    	 self.setState({ faceDetected:false })

		}
	 
	}

	completeMatch(nm, dist){

		self.setName(nm)
		self.setDistance(dist)

		self.setState({frameType:2})
		
	    // set timer
		interval2 = setInterval(self.progressToActiveMode, 500)
		
	}

	progressToActiveMode(){
		
		// clear interval
		clearInterval(interval)
		interval = null

		clearInterval(interval2)
		interval2 = null

		// continue
		store.dispatch({ type: 'face_detected', data:'true', name:self.getName(), distance:self.getDistance(), modeType:'face_detected', message:'welcome' })
	}

	render(){

		//console.log('update!!!!!')

		return(

			<div className='detectionHolder'>

			<div className='detectionVideo'><video className='video' muted id='video'><source src='' /></video></div>
			<div className='detectionCanvas'><canvas ref="canvas" /></div>
			<div>
			
			<img alt='img' ref="imageFrame" src={faceFrame} className="" />
			<img alt='img' ref="imagePepsi" src={pepsiImage} className="hidden" />
			<img alt='img' ref="imageRingsText" src={ringsTextImage} className="detectionIntro" />
			</div>
			<div className='trailer'><img alt='img' ref="person" src={emir} className="" />	</div>
			</div>

		)

	}

}
export default LoadVideoDetection