import React, { Component } from 'react';
import { store } from '../index.js';
import LoadAnimationCircular from '../components/LoadAnimationCircular'
import LoadAnimationSquare from '../components/LoadAnimationSquare'

import startButton from '../images/button_start.png'
import insta_image from '../images/insta_image.png'

const JSON_URL = process.env.PUBLIC_URL+'/json/social.json'
const axios = require('axios') // fetch doesn't work on iphone!

let self
let interval
let interval2
const startCount = -1 // start with Twitter Page ie 0
export class LoadSocialMedia extends Component {

	constructor(){

		super()

		self = this

		// initialise state
		self.state = {data:'', count:startCount, content:''}

	}

	componentDidMount(){

		// load data
		axios.get(JSON_URL)
	    .then(function (response) {

	    	let array = []

	    	// store in an array
	    	Object.keys(response.data).map(key => (

	    		array.push([response.data[key].name, response.data[key].img, response.data[key].text, response.data[key].type])

	    	))

	    	// set state
     	    self.setState({ data: array })

     	    // start timer
		    interval = setInterval(self.updateContent, 8000) // switch to next social screen

			interval2 = setInterval(self.returnToTrailers, 30000) // switch back to trailers	    

		    // get first item
		    self.updateContent()

	    })

	}

	componentWillUnmount(){

		clearInterval(interval)
		interval = null

		clearInterval(interval2)
		interval2 = null
	}

	returnToTrailers(){
		// console.log('return to trailer')
		store.dispatch({ type: 'trailer_update', trailerComplete:false })

	}

	updateContent(){

		let count = self.state.count
		let length = self.state.data.length

		if(count<length-1){

			count++
		}
		else{

			count = 0
		}

		self.setState({count:count})

	}

	startActive(){

		store.dispatch({ type: 'mode_type', data:'active' }) // for demo purposes active mode can only be accessed via this button!!!!

	}


	render(){

		let content
		let text
		
		// get content from array
		if(self.state.count>startCount){

			let array = self.state.data[self.state.count]

			text = <p dangerouslySetInnerHTML={{__html: array[2]}} />
			
			if(array[3]==='twitter'){

				content = <div><div className='socialAnimation'><LoadAnimationCircular /></div><div className='twitterContent'>{text}</div><div className='startButton' onClick={()=>self.startActive()}><img alt='start' src={startButton}	/></div></div>

			}else{

				content = <div><div className='socialAnimation'><LoadAnimationSquare /></div><div className='instagramContent'><img alt='instagram' src={insta_image} /><div className='instagramText'>{text}</div><div className='startButton' onClick={()=>self.startActive()}><img alt='start' src={startButton} /></div></div></div>

			}
		
		}

		return(

			<div className='social'>
			<h1 className='header'>VUE ON SOCIAL</h1>
			<h2 className='header2'>Tag us #vuespire to feature</h2>

			{content}
			
			
			</div>
		)

	}

}

export default LoadSocialMedia
