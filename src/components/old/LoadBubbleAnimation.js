import React, { Component } from 'react';
import ovalBlue from '../images/ovalBlue.png' 
import ovalGreen from '../images/ovalGreen.png'
import ovalOrange from '../images/ovalOrange.png'
import ovalYellow from '../images/ovalYellow.png'
import ovalGrey from '../images/ovalGrey.png'
import {TweenMax} from 'gsap';

let childrenBubbles;

export class LoadBubbleAnimation extends Component {

	componentDidMount(){

		for (let i = 0; i < this.props.bubbleNum; i++) {

			if(this.refs[i]){

          let repeatDelay = Math.random()*0.3

		      TweenMax.to(this.refs[i], 1, {scale:0.9, repeat:-1, yoyo:true, delay:1, repeatDelay:repeatDelay})
		    }
		}

	}

	render(){

		//let content = <img src={ovalGreen} />
		let imgArray = [ovalBlue, ovalGreen, ovalOrange, ovalGreen, ovalYellow, ovalGreen, ovalGrey, ovalOrange, ovalGreen, ovalGreen]
		childrenBubbles = []

		// random draw the four images onto the page & scale in & out
		for (let i = 0; i < this.props.bubbleNum; i++) {

      //let num = Math.round(Math.random()*(imgArray.length-1))
      //let img = <img alt='img' src={imgArray[num]} />

      let img = <img alt='img' src={imgArray[i]} />

			childrenBubbles.push(<div ref={i} key={i} style={stylesArray[i]}>{img}</div>)

		}

		return(

			<div>

				{childrenBubbles}
				

			</div>

		)

	}

}

//150,120
//130, 200
// 350, 250
// 300, 300

const styles = {
  bubbble1: {
  	position:'absolute',
  	marginLeft:300,
  	marginTop:90
  },
  bubbble2: {
  	position:'absolute',
  	marginLeft:130,
    marginTop:100
  },
  bubbble3: {
  	position:'absolute',
    marginLeft:450,
    marginTop:150
  },
  bubbble4: {
  	position:'absolute',
  	marginLeft:400,
    marginTop:200
  },
  bubbble5: {
  	position:'absolute',
  	marginLeft:200,
    marginTop:220
  },
  bubbble6: {
  	position:'absolute',
  	marginLeft:400,
    marginTop:630
  },
  bubbble7: {
    position:'absolute',
    marginLeft:180,
    marginTop:650
  },
  bubbble8: {
    position:'absolute',
    marginLeft:350,
    marginTop:680
  },
  bubbble9: {
    position:'absolute',
    marginLeft:120,
    marginTop:720
  },
  bubbble10: {
    position:'absolute',
    marginLeft:330,
    marginTop:750
  }

}

const stylesArray = [styles.bubbble1, styles.bubbble2, styles.bubbble3, styles.bubbble4, styles.bubbble5, styles.bubbble6, styles.bubbble7, styles.bubbble8, styles.bubbble9]

export default LoadBubbleAnimation