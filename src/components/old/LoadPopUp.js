import React, { Component } from 'react';
import {TweenMax} from 'gsap';
import { store } from '../index.js';

let self
const nextBoxTimer = 5
export class LoadPopUp extends Component {

	constructor(){

		super()

		self = this

		self.state = { popUp:'popUpNectar' }

	}

	componentDidMount(){

		TweenMax.fromTo(self.refs.popup, 0.5, {alpha:0}, {alpha:1, repeat:1, yoyo:true, repeatDelay:nextBoxTimer, onComplete:self.updatePopUp})

	}

	updatePopUp(){

		self.setState({ popUp: 'popUpRefills' })

		TweenMax.fromTo(self.refs.popup, 0.5, {alpha:0}, {alpha:1, repeat:1, yoyo:true, repeatDelay:nextBoxTimer, delay:1, onComplete:self.clearPopUp})
	}

	clearPopUp(){

		self.setState({ popUp: '' })

		store.dispatch({  type:'set_nectar_points', nectarPoints:0 }) // new drink

	}

	render(){

		return(
			<div className='popUpHolder'>
			<div ref='popup' className={this.state.popUp} ></div>		
			</div>
		)

	}

}
export default LoadPopUp