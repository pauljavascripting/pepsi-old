import React, { Component } from 'react';
import { store } from '../index.js';
import { LoadWelcome } from '../components/LoadWelcome'
import { LoadDrinksIntro } from '../components/LoadDrinksIntro'
import { LoadDrinksNew } from '../components/LoadDrinksNew'
import { LoadDrinksMenu } from '../components/LoadDrinksMenu'
import { LoadPopUp } from '../components/LoadPopUp'
import { LoadEndScreen } from '../components/LoadEndScreen'
import { DisplayNectarCup } from '../components/DisplayNectarCup'

let self
export class DisplayResponse extends Component {

	constructor(){

		super()

		self = this

		self.state = { displayCup:false }

	}

	componentDidMount(){

	}

	static getDerivedStateFromProps(nextProps){

	    if(nextProps.nectarPoints>0 && self.state.displayCup===false){

	    	self.setState({ displayCup:true })
	    }

	    return null

  }



	showDrinksIntro(){

		store.dispatch({ type: 'response_drinks_intro', pourDrink:'false', pouring:'false' }) // for demo purposes active mode can only be accessed via this button!!!!

	}

	render(){

		let content
		let popUp
		let cup
		
		if(self.props.nectarPoints>0){
		
			popUp = <LoadPopUp />

		}

		if(self.state.displayCup===true && self.props.response!=='response_drinks_endscreen'){

			cup = <DisplayNectarCup />
		}

		switch(self.props.response){

			case 'welcome':

				content = <LoadWelcome userName={self.props.userName} />

				let timer = setTimeout(self.showDrinksIntro, 5000) // timer to define when to display showDrinksIntro()

			break;

			case 'response_drinks_intro':

				content = <LoadDrinksIntro chosenDrinks={self.props.chosenDrinks} chosenFlavours={self.props.chosenFlavours} pourDrink={self.props.pourDrink} pouring={self.props.pouring} />

			break;

			case 'response_drink_same':

				content = <LoadDrinksIntro chosenDrinks={self.props.chosenDrinks} chosenFlavours={self.props.chosenFlavours} pourDrink={self.props.pourDrink} pouring={self.props.pouring}  />

			break;

			case 'response_drink_add':

				content = <LoadDrinksNew response={self.props.response} chosenDrinks={self.props.chosenDrinks} chosenFlavours={self.props.chosenFlavours} newDrink={self.props.newDrink} pourDrink={self.props.pourDrink} pouring={self.props.pouring}  />

			break;

			case 'response_create_new_drink':

				content = <LoadDrinksNew response={self.props.response} chosenDrinks={self.props.chosenDrinks} chosenFlavours={self.props.chosenFlavours} newDrink={self.props.newDrink} pourDrink={self.props.pourDrink} pouring={self.props.pouring}  />

			break;

			case 'response_drink_new':

				content = <LoadDrinksMenu  />

			break;

			case 'response_drinks_menu':

				content = <LoadDrinksMenu  />

			break;

			case 'response_drinks_endscreen':

				content = <LoadEndScreen chosenDrinks={self.props.chosenDrinks} chosenFlavours={self.props.chosenFlavours} />

			break;

			default:

			break;

		}

		return(

			<div>

			<div className='nectarCupHolder'> {cup}  </div>

			{ self.props.nectarPoints>0 ? <div className='popup'>{popUp}</div> : null }

			<div className='response'>
				
				{content}
				
			</div>

			</div>

		)

	}

}
export default DisplayResponse