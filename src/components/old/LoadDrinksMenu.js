import React, { Component } from 'react';
import {TweenMax, Elastic} from 'gsap';
import { store } from '../index.js';

// drink options
import bubble7up from '../images/bubble_7up.png' // 0
import bubbleDiet from '../images/bubble_diet.png' // 1
import bubbleMax from '../images/bubble_max.png' // 2
import bubblePepsi from '../images/bubble_pepsi.png' // 3
import bubbleRwhites from '../images/bubble_rwhites.png' // 4
import bubbleTango from '../images/bubble_tango.png' // 5

//import backButton from '../images/back+_.png'

// animation
import LoadBubbleAnimation from '../components/LoadBubbleAnimation';

let self
export class LoadDrinksMenu extends Component {

	constructor(){

		super()

		self = this
	}

	componentDidMount(){

		// tween drink 0 - tango
	    if(self.refs['0']){
	      TweenMax.fromTo(self.refs['0'], 1.5, {scale:0}, {scale:0.77, y:40, ease:Elastic.easeOut})  
	      TweenMax.to(self.refs['0'], 1, {scaleX:0.75, repeat:-1, yoyo:true, delay:1, repeatDelay:0.5})
	    }

	    // tween drink 1 - 7up
	    if(self.refs['1']){
	      TweenMax.fromTo(self.refs['1'], 1.3, {scale:0}, {scale:0.5, x:5, y:25, ease:Elastic.easeOut})  
	      TweenMax.to(self.refs['1'], 1.3, {scaleX:0.47, repeat:-1, yoyo:true, delay:0.9, repeatDelay:0.5})
	    }
	    
	    // tween drink 2 - pepsi
	    if(self.refs['2']){
	      TweenMax.fromTo(self.refs['2'], 1, {scale:0}, {scale:0.97, y:20, ease:Elastic.easeOut})  
	      TweenMax.to(self.refs['2'], 1.2, {scaleX:0.93, repeat:-1, yoyo:true, delay:1.1, repeatDelay:0.5})
	    }

	    // tween drink 3 - max
	    if(self.refs['3']){
	      TweenMax.fromTo(self.refs['3'], 1.5, {scale:0}, {scale:0.83, y:40, ease:Elastic.easeOut}) 
	      TweenMax.to(self.refs['3'], 1.1, {scaleX:0.8, repeat:-1, yoyo:true, delay:1, repeatDelay:0.5}) 
	    }

	    // tween drink 4 - rhwite
	    if(self.refs['4']){
	      TweenMax.fromTo(self.refs['4'], 1.3, {scale:0}, {scale:0.54, x:10, y:45, ease:Elastic.easeOut})  
	      TweenMax.to(self.refs['4'], 0.9, {scaleX:0.52, repeat:-1, yoyo:true, delay:0.9, repeatDelay:0.5})
	    }
	    
	    // tween drink 5 - diet
	    if(self.refs['5']){
	      TweenMax.fromTo(self.refs['5'], 1, {scale:0}, {scale:0.51, y:30, ease:Elastic.easeOut})  
	      TweenMax.to(self.refs['5'], 1.1, {scaleX:0.47, repeat:-1, yoyo:true, delay:1.2, repeatDelay:0.5})
	    }
	}

	goBack(){

		store.dispatch({ type: 'response_drinks_intro' }) // back to intro

	}

	selectDrink(drinkNum){

		store.dispatch({ type: 'response_drink_add', newDrink:drinkNum}) // new drink

	}
	
	render(){

		//<LoadTangoAnimation />

		let content = <div><div onClick={()=>self.goBack()} className='backButton'></div><div className='drinksMenuHolder1'><LoadBubbleAnimation bubbleNum={9} /></div><div className='drinksMenuHolder2'><img alt='drink' onClick={()=>self.selectDrink(5)} ref={0} className='drinkTango' src={bubbleTango} /><img alt='drink' onClick={()=>self.selectDrink(0)} ref={1} className='drink7up' src={bubble7up} /><img alt='drink' onClick={()=>self.selectDrink(3)} ref={2} className='drinkPepsi' src={bubblePepsi} /><img alt='drink' onClick={()=>self.selectDrink(2)} ref={3} className='drinkMax' src={bubbleMax} /><img alt='drink' onClick={()=>self.selectDrink(4)} ref={4} className='drinkRwhite' src={bubbleRwhites} /><img alt='drink' onClick={()=>self.selectDrink(1)} ref={5} className='drinkDiet' src={bubbleDiet} /></div></div>
		
		return(

			<div>

			{content}

			</div>

		)

	}

}
export default LoadDrinksMenu