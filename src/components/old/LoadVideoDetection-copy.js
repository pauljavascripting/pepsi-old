import React, { Component } from 'react';
import {TweenMax,Linear, Elastic} from 'gsap';
import * as faceapi from 'face-api.js';
import { store } from '../index.js';
import faceFrame from '../images/face_frame.png'
import pepsiImage from '../images/face_frame_pepsi.png'
import ringsTextImage from '../images/rings_text.png'

let video
let interval
let self
const faceDetectionScore = 0.35
const faceMatchScore = 0.5
const useTinyModel = true
const videoHeight = 888;
const videoWidth = 600;
const MODEL_URL = process.env.PUBLIC_URL+'/models'
const JSON_URL = process.env.PUBLIC_URL+'/json/faces.json'
let canvasPosX = 0
let canvasPosY = 0
let faceDistance = 0
let faceName = ''
const repositionAmt = 50

export class LoadVideoDetection extends Component {

	constructor(){

		super()

		this.state = {faceDetected:false, introImage:true}

		interval = null
		
		self = this
	}


	getName(){

		return faceName;
	}

	setName(nm){

		faceName = nm
	}

	getDistance(){

		return faceDistance

	}

	setDistance(dist){

		faceDistance = dist
	}

	getCanvasPositionX(){

		return canvasPosX-repositionAmt
	}

	getCanvasPositionY(){

		return canvasPosY-repositionAmt
	}

	setCanvasPositionX(x){

		canvasPosX = x
	}

	setCanvasPositionY(y){

		canvasPosY = y
	}

	componentDidUnMount(){

		clearInterval(interval)
		interval = null
	}

	async componentDidMount(){

		// load facial models ---------------------------------------
		await faceapi.loadTinyFaceDetectorModel(MODEL_URL)
	    await faceapi.loadFaceLandmarkTinyModel(MODEL_URL)
    	await faceapi.loadFaceRecognitionModel(MODEL_URL)

    	// add video ---------------------------------------
		var constraints = { audio: false, video: { width: videoWidth, height: videoHeight } }; 
	    navigator.mediaDevices.getUserMedia(constraints)
	    .then(function(mediaStream) {

	      //video = document.querySelector('video');
	      video = document.getElementById('video')
	      video.srcObject = mediaStream;
	        
	        // video on loaded ---------------------------------------
	        video.onloadedmetadata = function(e) {

	          // play video
	          video.play();

	        }
	    })

	}

	static getDerivedStateFromProps(nextProps){

		if(nextProps.modeType==='active' && !interval){

			interval = setInterval(self.drawBox, 500) // start calling drawBox() once passive mode has been removed

		}

		return null

	}

	async drawBox(){

		let input = video
    
	    const detections = await faceapi.detectAllFaces(input, new faceapi.TinyFaceDetectorOptions())
	    const detectionsForSize = faceapi.resizeResults(detections, { width: videoWidth, height: videoHeight })

	    // draw them into a canvas
	    // const canvas = document.getElementById('canvas')
	    // canvas.width = videoWidth
	    // canvas.height = videoHeight
	    // faceapi.drawDetection(canvas, detectionsForSize, { withScore: true })

	  	// draw frame onto the canvas
	    if(detectionsForSize[0]){

	    	self.setCanvasPositionX(detectionsForSize[0]._box._x)
		    self.setCanvasPositionY(detectionsForSize[0]._box._y)
		    
		    // draw image frame onto canvas
		    self.drawImageToCanvas(1,50,40)

		    // hide intro image -----------------------------------
		    if(self.state.introImage){
		    	let img = self.refs.imageRingsText
				img.style.visibility = 'hidden'
				self.setState({introImage:false})
		    }
		    

	    }
	    
	    if(detections[0] && detections[0]._score > faceDetectionScore && self.state.faceDetected===false){

	    	// update state - to prevent loop
	    	self.setState({ faceDetected:true })

	    	//console.log('END--------'+detections[0]._score)

	        // stop video
	       // video.pause()
	        
	        // clear interval
	        clearInterval(interval)
	        interval = null

	        // store results
	        let img1 = await faceapi
	        .detectAllFaces(video, new faceapi.TinyFaceDetectorOptions())
	        .withFaceLandmarks(useTinyModel)
	        .withFaceDescriptors();

	        // check for match
	        self.checkMatch(img1)

	    }

	}

	async checkMatch(img1){

		//console.log('checkMatch---------'+interval)
		//console.log(img1[0])

		if(img1[0]){

			 //add first image to facematch
		     const faceMatcher = new faceapi.FaceMatcher(img1)

			  const json = await faceapi.fetchJson(JSON_URL)

			  let bestMatch
			  let count = 0

			  // loop through json to detect match
			  // Object.keys(json).map(key => (

			  //     bestMatch = faceMatcher.findBestMatch(json[key].descriptors),
			      
			  //     (bestMatch._distance<faceMatchScore &&
			     
			  //       // successful match
			  //       self.completeMatch(json[key].name, bestMatch._distance)
			        
			  //     )
			  //     // ,(bestMatch._distance>0.4 &&
			  //     //   console.log(json[key].name+' '+bestMatch._distance)
			  //     // )
			       
			  // ))

			  Object.keys(json).map(function(key,i){

			  		bestMatch = faceMatcher.findBestMatch(json[key].descriptors)

			  		if(bestMatch._distance<faceMatchScore){

			  			self.completeMatch(json[key].name, bestMatch._distance)

			  			return
			  		}

			  		if(i==Object.keys(json).length-1){

			  			console.log('no match!!!')

			  			// restart video
		         		video.play()
		        
		         		// redraw box
		        		interval = setInterval(self.drawBox, 500)

		         		// disable
		    	 		self.setState({ faceDetected:false })
			  		}

			  })
			  
			
		}
		else{

			console.log('restart--------')

			 // restart video
	         video.play()
	        
	         // redraw box
	         interval = setInterval(self.drawBox, 500)

	         // disable
	    	 self.setState({ faceDetected:false })

		}
	 
	}

	completeMatch(nm, dist){

		self.setName(nm)
		self.setDistance(dist)
		
		self.drawImageToCanvas(2, 80, 92) // add pepsi frame

	    // set timer
		interval = setInterval(self.progressToActiveMode, 2000)
		
	}

	drawImageToCanvas(num, incrementX, incrementY){

		let img

		// draw image frame onto canvas
	    const canvas = self.refs.canvas
	    const ctx = canvas.getContext("2d");
	    canvas.width = videoWidth
	    canvas.height = videoHeight
	    
	    switch(num){

	    	// case 0:

	    	// 	 img = self.refs.imageRingsText
	    		
	    	// break;

	    	case 1:

	    		 img = self.refs.imageFrame

	    	break;

	    	case 2:

	    		 img = self.refs.imagePepsi

	    	break;


	    }
	    
	    // draw image
	    ctx.drawImage(img, self.getCanvasPositionX()-incrementX, self.getCanvasPositionY()-incrementY);

	    // tween
	    TweenMax.killAll()
	    TweenMax.fromTo(canvas, 0.2, {alpha:0}, {alpha:1})

	}

	progressToActiveMode(){
		
		// clear interval
		clearInterval(interval)
		interval = null
		
		// continue
		store.dispatch({ type: 'face_detected', data:'true', name:self.getName(), distance:self.getDistance(), modeType:'face_detected', message:'welcome' })
	}

	render(){

		console.log('update!!!!!')

		return(

			<div className='detectionHolder'>

			<div className='detectionVideo'><video muted id='video'><source src='' /></video></div>
			<div className='detectionCanvas'><canvas ref="canvas" /></div>
			<div>
			<img ref="imageFrame" src={faceFrame} className="" />
			<img ref="imagePepsi" src={pepsiImage} className="hidden" />
			<img ref="imageRingsText" src={ringsTextImage} className="detectionIntro" />
			</div>
			</div>

		)

	}

}
export default LoadVideoDetection