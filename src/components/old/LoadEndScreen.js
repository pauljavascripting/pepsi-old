import React, { Component } from 'react';
import { store } from '../index.js';

let div
let timer
let self
export class LoadEndScreen extends Component {

	constructor(){

		super()

		self = this
	}

	componentDidMount(){

		div = document.getElementsByClassName('mainContent')
		
		if(div){
			div[0].style.backgroundColor = 'black'	
		
		}

		// timer
		timer = setTimeout(this.returnToStart, 20000)
		
	}

	componentWillUnmount(){

		div[0].style.backgroundColor = 'transparent'	
		
	}

	returnToStart(){

		clearTimeout(timer)
		timer = null

		store.dispatch({ type: 'demo', data:'passive', responseMessage:'welcome', chosenDrinks:self.props.chosenDrinks, newDrink:2, chosenFlavours:self.props.chosenFlavours, pourDrink:false, pouring:false, nectarPoints:0, endScreen:false, trailerComplete:false  })
	}

	render(){

		return(

			<div className='endScreen'>

			<div className='endText1'>YOU'VE WON TICKETS TO OUR CHAMPIONS LEAGUE SCREENING</div>

			<div className='endText2'>Enjoy the film, then check your Vue account for the tickets.</div>			

			</div>

		)

	}

}
export default LoadEndScreen