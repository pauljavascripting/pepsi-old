import React, { Component } from 'react';
import {TweenMax,Linear, Elastic} from 'gsap';
import { store } from '../index.js';
import makeNew from '../images/button_makenew.png'
import pour from '../images/button_pour.png'
import pouring from '../images/button_pouring.png'

// drink options
import bubble7up from '../images/bubble_7up.png' // 0
import bubbleDiet from '../images/bubble_diet.png' // 1
import bubbleMax from '../images/bubble_max.png' // 2
import bubblePepsi from '../images/bubble_pepsi.png' // 3
import bubbleRwhites from '../images/bubble_rwhites.png' // 4
import bubbleTango from '../images/bubble_tango.png' // 5

// flavour options
import cherryFlavour from '../images/flavourCherryOff.png' // 0
import vanillaFlavour from '../images/flavourVanillaOff.png' // 1
import limeFlavour from '../images/flavourLimeOff.png' // 2

// animation
import LoadWaterAnimation from '../components/LoadWaterAnimation';
import LoadBubbleAnimation from '../components/LoadBubbleAnimation';

// *************************************************
//
// NB Scaling of the drink bubbles is now
// controlled by the tweens in componentDidMount()
//
//
//
// *************************************************

let self
export class LoadDrinksIntro extends Component {

	constructor(){

		super()

		self = this
	}

  componentDidMount(){

    
    // tween drink 0
    if(self.refs['0']){
      TweenMax.fromTo(self.refs['0'], 1.5, {scale:0}, {scale:1.1, ease:Elastic.easeOut})  
      TweenMax.to(self.refs['0'], 1, {scaleX:1, repeat:-1, yoyo:true, delay:1, repeatDelay:0.5})
    }

    // tween drink 1
    if(self.refs['1']){
      TweenMax.fromTo(self.refs['1'], 1.3, {scale:0}, {scale:0.75, ease:Elastic.easeOut})  
      TweenMax.to(self.refs['1'], 1.3, {scaleX:0.71, repeat:-1, yoyo:true, delay:0.9, repeatDelay:0.5})
    }
    
    // tween drink 2
    if(self.refs['2']){
      TweenMax.fromTo(self.refs['2'], 1, {scale:0}, {scale:0.6, ease:Elastic.easeOut})  
      TweenMax.to(self.refs['2'], 1.2, {scaleX:0.55, repeat:-1, yoyo:true, delay:1.1, repeatDelay:0.5})
    }

    // tween drink 3
    if(self.refs['3']){
      TweenMax.fromTo(self.refs['3'], 1.5, {scale:0}, {scale:0.55, ease:Elastic.easeOut}) 
      TweenMax.to(self.refs['3'], 1.1, {scaleX:0.51, repeat:-1, yoyo:true, delay:1, repeatDelay:0.5}) 
    }

    // tween drink 4
    if(self.refs['4']){
      TweenMax.fromTo(self.refs['4'], 1.3, {scale:0}, {scale:0.4, ease:Elastic.easeOut})  
      TweenMax.to(self.refs['4'], 0.9, {scaleX:0.41, repeat:-1, yoyo:true, delay:0.9, repeatDelay:0.5})
    }
    
    // tween drink 5
    if(self.refs['5']){
      TweenMax.fromTo(self.refs['5'], 1, {scale:0}, {scale:0.52, ease:Elastic.easeOut})  
      TweenMax.to(self.refs['5'], 1.1, {scaleX:0.45, repeat:-1, yoyo:true, delay:1.2, repeatDelay:0.5})
    }

    // tween flavour 1
    if(self.refs['6']){
      TweenMax.fromTo(self.refs['6'], 1.5, {scale:0}, {scale:0.8, ease:Elastic.easeOut})  
    }

    // tween flavour 2
    if(self.refs['7']){
      TweenMax.fromTo(self.refs['7'], 1, {scale:0}, {scale:0.8, ease:Elastic.easeOut})  
    }

     // tween flavour 3
    if(self.refs['8']){
      TweenMax.fromTo(self.refs['8'], 1.2, {scale:0}, {scale:0.8, ease:Elastic.easeOut})  
    }

  }

	makeNew(){
    
		store.dispatch({ type: 'response_drink_new', action:'new_drink' }) // new drink

	}

	makeSame(){
     
		store.dispatch({ type: 'response_drink_same', action:'same_drink', pourDrink:true }) // same drink
    
	}

	startPour(){
      
      store.dispatch({ type: 'response_drinks_intro',  pourDrink:true, pouring:true })

      // start descent water animation
      // on complete, display end screen
      if(self.refs['water']){

        TweenMax.to(self.refs['water'], 4, {y:200, ease:Linear.easeOut, onComplete:self.ShowEndScreen}) 
      }
	}

  ShowEndScreen(){

    store.dispatch({ type: 'response_drinks_endscreen',  endScreen:true })

  }

  stopPour(){

      TweenMax.to(self.refs['water'], 1, {y:0, ease:Linear.easeOut, onComplete:self.pouringReset}) 
    
  }

  pouringReset(){

    store.dispatch({ type: 'response_drinks_intro', pourDrink:true, pouring:false })

  }

	goBack(){
    // console.log('goback')
		store.dispatch({ type: 'response_drinks_intro', pourDrink:false })
	}

  goBackToDetection(){
    // console.log('goBackToDetection')
    store.dispatch({ type: 'mode_type', data:'active' })
  }

	render(){

		const childrenDrinks = [];
		const childrenFlavours = [];
    const drinksTitles = []
		//if(self.drinksHolder){

			// choose drinks ----------------------------------------------------------------------
			Object.keys(self.props.chosenDrinks).map(function(key,i){

        drinksTitles.push(drinksTitleArray[self.props.chosenDrinks[key]])

				childrenDrinks.push(<div ref={i} key={key} style={drinksStylesArray[i]}><img alt='drink' src={drinksArray[self.props.chosenDrinks[key]]} /></div>)
        
			})

			// choose flavours ----------------------------------------------------------------------
			Object.keys(self.props.chosenFlavours).map(function(key,i){

				childrenFlavours.push(<div ref={6+i} key={key} style={flavoursStylesArray[i]}><img alt='drink' src={flavoursArray[self.props.chosenFlavours[key]]} /></div>)
			})

		//}

    let bgHolder
		let button
    let backButton
		let endMessage
    let headerText
    let headerText2
    let water
   
    // console.log(self.props.pourDrink)
    // console.log(self.props.pouring)

		if(self.props.pourDrink===true){
      // console.log('1')
      bgHolder = 'drinksHolderBG1'
      headerText = drinksTitles.toString()
      headerText2 = <h2 className='header2'></h2>
      water = <div ref='water'><LoadWaterAnimation /></div>
			
      if(self.props.pouring===true){
        // console.log('2')
        button = <div className='drinksText0'><img alt='drink' src={pouring} /></div>
        endMessage = <div onClick={()=>self.stopPour()} className='stopDrink'></div>

      }
      else{
        // console.log('3')
        button = <div className='drinksButton1' onClick={()=>self.startPour()} ><img alt='drink' src={pour} /></div>
      }
		  
      backButton = <div onClick={()=>self.goBack()} className='backButton'></div>

		}
		else{
      // console.log('4')
      bgHolder = 'drinksHolderBG'
      headerText = 'YOUR LAST CREATION'
      backButton = <div onClick={()=>self.goBackToDetection()} className='backButton'></div>
      headerText2 = <h2 className='header2'>Grab your drink before<br />the film starts</h2>
			button = <div className='drinksButton' onClick={()=>self.makeNew()}><img alt='drink' src={makeNew} /></div>
			endMessage = <div className='drinksText1'>Create a new flavour<br />combination</div>

		}
		
    let content = <div className='contentHolder'>{backButton}<div className="introHolder"><h1 className='header'>{headerText}</h1>{headerText2}<div className={bgHolder} ref={drinksHolder => self.drinksHolder = drinksHolder}><div className='drinksHolder0'>{water}</div><div onClick={()=>self.makeSame()}><div className='drinksHolder1'>{childrenDrinks}</div><div className='drinksHolder2'>{childrenFlavours}</div></div>{button}{endMessage}</div></div></div>
    
		return(

      <div>

        <div className='drinksHolder'>

          <LoadBubbleAnimation bubbleNum={9} />

        </div>

  			 <div className='drinsksHolder1'>
  			{content}
  			</div>

      </div>

		)

	}

}

const styles = {
  bubble1: {
  	position:'absolute',
  	transform:'scale(1)',
  	marginLeft:150,
  	marginTop:170
  },
  bubble2: {
  	position:'absolute',
  	transform:'scale(0.8)',
  	marginLeft:240,
  	marginTop:100
  }, 
   bubble3: {
  	position:'absolute',
  	transform:'scale(0.7)',
  	marginLeft:60,
  	marginTop:210
  },
   bubble4: {
  	position:'absolute',
  	transform:'scale(0.55)',
  	marginLeft:350,
  	marginTop:170
  }, 
   bubble5: {
  	position:'absolute',
  	transform:'scale(0.4)',
  	marginLeft:70,
  	marginTop:315
  },
   bubble6: {
  	position:'absolute',
  	transform:'scale(0.52)',
  	marginLeft:250,
  	marginTop:300
  },
  bubbleFlavour1:{
  	position:'absolute',
  	transform:'scale(0.9)',
  	marginLeft:-50,
  	marginTop:90
  },
  bubbleFlavour2:{
  	position:'absolute',
  	transform:'scale(0.85)',
  	marginLeft:150,
  	marginTop:90
  },
  bubbleFlavour3:{
  	position:'absolute',
  	transform:'scale(0.9)',
  	marginLeft:50,
  	marginTop:110
  }
}

// styles array
const drinksStylesArray = [styles.bubble1, styles.bubble2, styles.bubble3, styles.bubble4, styles.bubble5, styles.bubble6]

// styles array
const flavoursStylesArray = [styles.bubbleFlavour1,styles.bubbleFlavour2, styles.bubbleFlavour3]

// drinks arrray
const drinksArray = [bubble7up, bubbleDiet, bubbleMax, bubblePepsi, bubbleRwhites, bubbleTango];

// drinks titles arrray
const drinksTitleArray = ['7UP', 'DIET PEPSI', 'PEPSI MAX', 'PEPSI', 'RWHITES', 'TANGO'];

// flavours array
const flavoursArray = [cherryFlavour, vanillaFlavour, limeFlavour];

export default LoadDrinksIntro