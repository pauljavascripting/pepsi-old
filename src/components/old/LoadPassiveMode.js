import React, { Component } from 'react';
import LoadSocialMedia from '../components/LoadSocialMedia'
import LoadTrailer from '../components/LoadTrailer'

let interval
let self
export class LoadPassiveMode extends Component {

	constructor(){

		super()

		self = this

		// state can be video or social
		self.state = { type:'video' }
	}

	componentDidMount(){

		interval = setInterval(self.moveToSocial, 5000)

	}

	moveToSocial(){

		clearInterval(interval)
		self.videoComplete()
	}

	videoComplete(){

		self.setState({ type:'social' })
	}

	render(){

		//onClick={()=>this.props.updateStatus()}

		let content

		if(this.props.mode_type==='passive'){

			if(this.state.type==='video'){
				
				content = <LoadTrailer />
			}
			else{

				content = <LoadSocialMedia />

			}
		}

		return(

			<div>{content}</div>

		)

	}

}
export default LoadPassiveMode