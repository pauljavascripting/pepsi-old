import React, { Component } from 'react';
import LoadAnimationCircular from '../components/LoadAnimationCircular'

export class LoadWelcome extends Component {

	render(){

		let content = <div className='welcomeHolder'><div className='welcomeAnimation'><LoadAnimationCircular /></div><div className='welcomeContent'>Hey {this.props.userName}, welcome back!<br /> We're getting your Pepsi favourites....</div></div>

		return(

			<div>
			{content}
			</div>

		)

	}

}
export default LoadWelcome