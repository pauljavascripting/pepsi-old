import React, { Component } from 'react';
import Lottie from 'react-lottie';
import animationData from '../animationJson/waterAnimation.json'

export class LoadWaterAnimation extends Component {

	render(){

		return(

			<div>

				<Lottie options={defaultOptions}/>

			</div>

		)

	}

}

const defaultOptions = {
  loop: true,
  autoplay: true, 
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  }
};

export default LoadWaterAnimation