import React, { Component } from 'react';
import { store } from '../index.js';

import alita from '../videos/Alita_trailer.mp4'
import fantasticBeasts from '../videos/FantasticBeasts_trailer.mp4'
import lego2 from '../videos/Lego2_trailer.mp4'

let div1
let div2
let video
let self
export class LoadTrailer extends Component {

	constructor(){

		super()
		self = this
		self.state = {arrayNum:0, array:[lego2, fantasticBeasts, alita, lego2]}

	}

	componentDidMount(){

		div1 = document.getElementsByClassName('mainContent')
		if(div1){
			div1[0].style.backgroundColor = 'black'	// change background colour
		}

		div2 = document.getElementsByClassName('headerLogo')
		if(div2){

			div2[0].style.visibility = 'hidden' // hide logos

		}

		self.setVideo()

		//let timer = setTimeout(self.continue, 10000)


	}
	setVideo(){

		let vid = self.refs.video

		if(vid){	

     		vid.src = self.state.array[self.state.arrayNum]
     		
			vid.play();
			// if(vid.muted){
			// 	vid.muted = false;
			// }

			vid.onwaiting = function() {
			   
			  // _this.setState({showLoader:true})
			
			};

			vid.oncanplaythrough = function() {
			   
			  // _this.setState({showLoader:false})
			    
			};

			vid.onended = function(){

				// update array number ----------------------------------------
	         	let num = self.state.arrayNum;
	         	if(num<self.state.array.length-1){

	         		num++;

	         	}
	         	else{

	         		num = 0;
	         	}

	         	// update state ------------------------------------------------
	         	self.setState({arrayNum:num});

	         	 // 8. next video ----------------------------------------
	         	 let url = self.state.array[self.state.arrayNum];
	         	
	         	 if(vid){
	         	 	vid.src = url;
	         	 	vid.play()
	         	 }
			}

		}

	}

	// continue ----------------------------
	continue(){
		// console.log('go to social')
		let vid = self.refs.video
		vid.pause()
		store.dispatch({ type: 'trailer_update', trailerComplete:true })
	}


	componentWillUnmount(){

		div1[0].style.backgroundColor = 'transparent'	

		div2[0].style.visibility = 'visible'
	}

	render(){

		return(
			
			<div className='trailer'><video onClick={()=>this.continue()}  className='video' ref='video'><source src='' /></video></div>

		)

	}

}
export default LoadTrailer