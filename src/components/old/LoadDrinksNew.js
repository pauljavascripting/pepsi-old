import React, { Component } from 'react';
import { store } from '../index.js';

// drink options
import bubble7up from '../images/bubble_7up.png' // 0
import bubbleDiet from '../images/bubble_diet.png' // 1
import bubbleMax from '../images/bubble_max.png' // 2
import bubblePepsi from '../images/bubble_pepsi.png' // 3
import bubbleRwhites from '../images/bubble_rwhites.png' // 4
import bubbleTango from '../images/bubble_tango.png' // 5

import {TweenMax,Sine, Linear} from 'gsap';

// animation
import LoadBubbleAnimation from '../components/LoadBubbleAnimation';
import LoadWaterAnimation from '../components/LoadWaterAnimation';

let self
let bubbles
let timer
export class LoadDrinksNew extends Component {

	constructor(){

		super()

		self = this

    self.state = { cherryButton:'flavourCherryOff', vanillaButton:'flavourVanillaOff', limeButton:'flavourLimeOff', newFlavoursArray:[], flavoursArray:[], drinksArray:[], savedButton:'saveDrink', initialLoad:true }
	}

  componentDidMount(){

    //console.log(self.props.chosenDrinks)

    // store here to prevent reloading on every button click
    bubbles = <LoadBubbleAnimation bubbleNum={5} />

    // store current saved drinks
    self.setState({ drinksArray: self.props.chosenDrinks })

     // store current saved flavours
    self.setState({ flavoursArray: self.props.chosenFlavours })

    // tween drink -------------------------------
    if(self.drinksHolder){
      TweenMax.fromTo(self.drinksHolder, 0.3, {x:-50, y:150}, {x:0, y:0, ease:Sine.easeOut})  
      TweenMax.to(self.drinksHolder, 1, {scaleX:0.95, repeat:-1, yoyo:true, delay:0.5, repeatDelay:0.15})
    }

    // tween cherry
    if(self.refs['0']){

     TweenMax.to(self.refs['0'], 1, {scale:0.9, repeat:-1, yoyo:true, delay:1, repeatDelay:0.15})
    }

    // tween vanilla
    if(self.refs['1']){

     TweenMax.to(self.refs['1'], 1, {scale:0.92, repeat:-1, yoyo:true, delay:1, repeatDelay:0.16})
    }

    // tween lime
    if(self.refs['2']){

     TweenMax.to(self.refs['2'], 1, {scale:0.94, repeat:-1, yoyo:true, delay:1, repeatDelay:0.15})
    }

  }

	goBack(){
    // console.log('back')
		store.dispatch({ type: 'response_drinks_menu' }) // new drink

	}

	saveDrink(){

    store.dispatch({ type: 'response_create_new_drink' }) // new drink

	}

  static getDerivedStateFromProps(nextProps){

    if(nextProps.response==='response_create_new_drink'){ // once drinks arrays have been update in the store, then complete save of new drink

      self.completeSaveDrink()

    }
    

    return null

  }

  completeSaveDrink(){

     // if drink not already stored, then store
    if(self.state.drinksArray.indexOf(self.props.newDrink)===-1){

      let drinksArrayNew = []//self.state.drinksArray
      drinksArrayNew.push(self.props.newDrink)

      self.setState({drinksArray: drinksArrayNew, savedButton:'savedDrink', initialLoad:false}) // display saved button

    }

    // update store
    store.dispatch({ type: 'update_flavours_and_drinks', chosenFlavours:self.state.newFlavoursArray, chosenDrinks:self.state.drinksArray })

  }

  updateCherry(){

    let array = self.state.newFlavoursArray

    if(self.state.cherryButton==='flavourCherryOn'){

      let index = array.indexOf(0)
      array.splice(index, 1)  // remove from array

      self.setState({ cherryButton:'flavourCherryOff', initialLoad:false, newFlavoursArray:array })

     
    }
    else{

      array.push(0)  // add to array

      self.setState({ cherryButton: 'flavourCherryOn', initialLoad:false,  newFlavoursArray:array })

      
    }

  }

  updateVanilla(){

     let array = self.state.newFlavoursArray

    if(self.state.vanillaButton==='flavourVanillaOn'){

      let index = array.indexOf(1) // remove from array
      array.splice(index, 1)

      self.setState({ vanillaButton:'flavourVanillaOff', initialLoad:false, newFlavoursArray:array })

    }
    else{

      array.push(1) // add to array

      self.setState({ vanillaButton: 'flavourVanillaOn', initialLoad:false, newFlavoursArray:array })

    }

  }

  updateLime(){

    let array = self.state.newFlavoursArray

    if(self.state.limeButton==='flavourLimeOn'){

        let index = array.indexOf(2)  // remove from array
        array.splice(index, 1)

        self.setState({ limeButton:'flavourLimeOff', initialLoad:false, newFlavoursArray:array })

    }
    else{

      array.push(2)  // add to array

      self.setState({ limeButton: 'flavourLimeOn', initialLoad:false, newFlavoursArray:array})

    }

  }

	pourDrink(){

      // descend water
      TweenMax.to(self.refs['water'], 4, {y:200, ease:Linear.easeOut}) 

      // shrink button
      TweenMax.to(self.refs.pourDrink, 0.5,{'background-size':'120px 122px'});
	}

  pourDrinkPause(){

    // ascend water
    TweenMax.to(self.refs['water'], 1, {y:0, ease:Linear.easeOut}) 

    // reset button
     TweenMax.to(self.refs.pourDrink, 0.5,{'background-size':'149px 151px'});

    // end screen
    timer = setTimeout(self.ShowEndScreen, 1000)
  }

  ShowEndScreen(){

    store.dispatch({ type: 'response_drinks_endscreen',  endScreen:true })

  }

	render(){

		const childrenDrinks = [];
    let array
   
    // if save button is visible
    if(self.state.savedButton==='saveDrink'){

      array = drinksStylesArray

    }
    else{

      // drink saved notification is visible
      array = drinksStylesArrayBig
    }

		

    let cherryButton = <div className='cherryButton'><div ref={0} onClick={()=>self.updateCherry()} className={self.state.cherryButton}></div>Cherry</div>

    let vanillaButton = <div className='vanillaButton'><div ref={1} onClick={()=>self.updateVanilla()} className={self.state.vanillaButton}></div>Vanilla</div>

    let limeButton = <div className='limeButton'><div ref={2} onClick={()=>self.updateLime()} className={self.state.limeButton}></div>Lime</div>
 
		let content = <div><div onClick={()=>self.goBack()} className='backButton'></div><div className='drinkHolderSingle' ref={drinksHolder => self.drinksHolder = drinksHolder}><img alt='drink' className='drinkSingle' src={drinksArray[self.props.newDrink]} /></div><div className='addFlavour'>ADD FLAVOUR</div><div className='flavoursHolder'>{ cherryButton }{vanillaButton}{limeButton}</div><div onClick={()=>self.saveDrink()} className={self.state.savedButton}>{childrenDrinks}</div><div onMouseUp={()=>self.pourDrinkPause()} onMouseDown={()=>self.pourDrink()} ref='pourDrink' className='pourDrink'>Place your cup below</div></div>

		return(

			<div>

        <div className='drinksHolder'>

          {bubbles}

        </div>

        <div className='drinksHolder0'>

         <div ref='water'><LoadWaterAnimation /></div>

        </div>

        <div className='drinsksHolder1'>

        {content}

        </div>

      </div>

		)

	}

}

const styles = {
  bubble7up: {
  	position:'absolute',
  	transform:'scale(0.15)',
  	marginLeft:50,
  	marginTop:-130
  },
  bubbleDiet: {
  	position:'absolute',
  	transform:'scale(0.25)',
  	marginLeft:250,
  	marginTop:-120
  }, 
   bubbleMax: {
  	position:'absolute',
  	transform:'scale(0.21)',
  	marginLeft:150,
  	marginTop:-130
  },
   bubblePepsi: {
  	position:'absolute',
  	transform:'scale(0.21)',
  	marginLeft:30,
  	marginTop:-70
  }, 
   bubbleRwhites: {
  	position:'absolute',
  	transform:'scale(0.16)',
  	marginLeft:150,
  	marginTop:-50
  },
   bubbleTango: {
  	position:'absolute',
  	transform:'scale(0.21)',
  	marginLeft:250,
  	marginTop:-50
  },

   bubble7upBig: {
    position:'absolute',
    transform:'scale(0.15)',
    marginLeft:200,
    marginTop:-20
  },
  bubbleDietBig: {
    position:'absolute',
    transform:'scale(0.25)',
    marginLeft:300,
    marginTop:-140
  }, 
   bubbleMaxBig: {
    position:'absolute',
    transform:'scale(0.21)',
    marginLeft:90,
    marginTop:-160
  },
   bubblePepsiBig: {
    position:'absolute',
    transform:'scale(0.21)',
    marginLeft:-40,
    marginTop:-80
  }, 
   bubbleRwhitesBig: {
    position:'absolute',
    transform:'scale(0.16)',
    marginLeft:50,
    marginTop:-20
  },
   bubbleTangoBig: {
    position:'absolute',
    transform:'scale(0.21)',
    marginLeft:320,
    marginTop:-70
  },
}

// styles array
const drinksStylesArray = [styles.bubble7up, styles.bubbleDiet, styles.bubbleMax, styles.bubblePepsi, styles.bubbleRwhites, styles.bubbleTango]

const drinksStylesArrayBig = [styles.bubble7upBig, styles.bubbleDietBig, styles.bubbleMaxBig, styles.bubblePepsiBig, styles.bubbleRwhitesBig, styles.bubbleTangoBig]

// drinks arrray
const drinksArray = [bubble7up, bubbleDiet, bubbleMax, bubblePepsi, bubbleRwhites, bubbleTango];

export default LoadDrinksNew