import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../semantic/dist/semantic.min.css';
import { LoadVideoDetection } from '../components/LoadVideoDetection';
// import { DisplayResponse } from '../components/DisplayResponse'
// import { LoadTrailer } from '../components/LoadTrailer'
// import { LoadSocialMedia } from '../components/LoadSocialMedia'
import imageVuePepsi from '../images/vue_x_pepsi.png'
import imageVuePepsiWhite from '../images/vue_x_pepsi_white.png'

// delete after testing
// import { LoadDrinksIntro } from '../components/LoadDrinksIntro'
// import { LoadDrinksNew } from '../components/LoadDrinksNew'
// import { LoadDrinksMenu } from '../components/LoadDrinksMenu'
// import { LoadEndScreen } from '../components/LoadEndScreen'

const nectarPointsAmt = 23;

//**************************************************************************
// This app :
// uses image detection using https://github.com/justadudewhohacks/face-api.js/
// is controlled by a redux store (./index.js)
// has it's main component in ../containers/App.js
// loads the face detection models, faces.json & social.json via the public folder
// listens for a RFID code via LoadPopUp()
// loads most styles via  via semantic / src / themes / globals / site.overrides
//
// 1. Load trailer (LoadTrailer())
// 2. Loads social (LoadSocialMedia / LoadAnimationCircular / LoadAnimationSuare)
// 3. Load video detection (LoadVideoDetection())
// 4. Loads Welcome 
// 5. Displays active mode via DisplayMode() (LoadDrinksIntro() / LoadDrinksMenu() / LoadDrinksNew)
//**************************************************************************

let self
// let interval
class App extends Component {

  constructor(){

    super()

    self = this

    self.state = {trailer_complete:false}

    self.handleInputTextChange = this.handleInputTextChange.bind(this)

  }

  handleInputTextChange(e){
   //console.log(e.target.value)
    if(e.target.value==='http://redirects.io.tt/pepsi/spire/ucl/'){ // if 'http://redirects.io.tt/pepsi/spire/ucl/' is inputted, display popup

      // update redux
      self.props.setNectarPoints(nectarPointsAmt)

      // reset
      e.target.value = ''
    }

  }

  static getDerivedStateFromProps(nextProps){

     // keep hidden textbox in focus -------------------------------------
    if(document.getElementById("textBox")){

      document.getElementById("textBox").focus()   
    }

    return null

  }


  render() {
 
    // display -------------------------------------
    let content
    let videoContent
    let logo

    // if(self.props.response_message==='response_drinks_endscreen'){

    //   logo = imageVuePepsiWhite // display white logo on end screen
    // }
    // else{

    //   logo = imageVuePepsi

    // }
    
    // if(self.props.mode_type==='face_detected'){

    //   content = <DisplayResponse userName={self.props.user_name} response={self.props.response_message} chosenDrinks={self.props.chosen_drinks} chosenFlavours={self.props.chosen_flavours} newDrink={self.props.new_drink} pourDrink={self.props.pour_drink} nectarPoints={self.props.nectar_points} pouring={self.props.pouring} endScreen={self.props.end_screen} />
    //   //videoContent = <div className='show'><LoadVideoDetection modeType={self.props.mode_type} /></div>  
    // }
    
    // if(self.props.mode_type==='active'){
      
    //   // content = <LoadTrailer />
    //   videoContent = <div className='show'><LoadVideoDetection modeType={self.props.mode_type} /></div>

    // }

    // if(self.props.mode_type==='passive' && self.props.trailer_complete===false){

    //   content = <LoadTrailer />
    //   videoContent = <div className='hidden'><LoadVideoDetection modeType={self.props.mode_type} /></div>

    // }

    // if(self.props.mode_type==='passive' && self.props.trailer_complete===true){

    //   content = <LoadSocialMedia />
    //   videoContent = <div className='hidden'><LoadVideoDetection modeType={self.props.mode_type} /></div>

    // }
    
  // content = <LoadTrailer  />
   videoContent = <div className='show'><LoadVideoDetection modeType={self.props.mode_type} /></div>

    return (
       <div className='ui grid'>
          
          <div className='ui centered eight wide column row mainContent'>

            <div className="ui row mainContentHolder">

               <div className='ui grid'>

               <div className="ui sixteen wide column headerLogo"><img alt='img' src={ logo } /></div>

               </div>

                <div className="ui left aligned sixteen wide column">{ content } {videoContent}</div>
                <div className="ui left aligned sixteen wide column"><div className='textBoxHolder'><input onChange={(e) => {self.handleInputTextChange(e)}} id='textBox' type="text" /></div></div>
            
            </div>

          </div>
       
      </div>
    );
  }
}

const mapStateToProps = (state, dispatch) => {

  return {

      mode_type:state.mode_type,
      user_name:state.user_name,
      chosen_drinks:state.chosen_drinks,
      chosen_flavours:state.chosen_flavours,
      new_drink:state.new_drink,
      nectar_points: state.nectar_points,
      response_message:state.response_message,
      pour_drink:state.pour_drink,
      pouring:state.pouring,
      end_screen:state.end_screen,
      trailer_complete:state.trailer_complete
  }

}

const mapDispatchToProps = dispatch => {
  return { 

    startActiveMode: () => dispatch({ type: 'mode_type', data:'active' }),
    setNectarPoints: (num) => dispatch({ type: 'set_nectar_points', nectarPoints:num }),

   }
}


// Connect Redux to App ---------------------------------------------------------------------------------- //
export default connect(mapStateToProps, mapDispatchToProps)(App);
